import os
import mysql.connector
from mysql.connector import Error


def ping(host):
    response = os.system('ping -c 1 ' + host)

    if response == 0:
        return host + ' *is up!*'
    else:
        return host + ' *is down!*'

def allHosts():
    connection = connect()
    cursor = connection.cursor()
    query = "SELECT name FROM websites"

    cursor.execute(query)
    result = cursor.fetchall()

    return result


def add(host):
    connection = connect()

    if connection == 0:
        return 'No database Connection'

    cursor = connection.cursor()
    query = "INSERT INTO websites (name) VALUES ('{0}')".format(host)

    cursor.execute(query)
    connection.commit()

    if cursor.rowcount > 0:
        return 'Site added successfully!'
    else:
        return 'Something went wrong !'


def remove(host):
    connection = connect()

    if connection == 0:
        return 'No database Connection'

    cursor = connection.cursor()
    query = "DELETE FROM websites WHERE name = '{0}'".format(host)

    cursor.execute(query)
    connection.commit()

    if cursor.rowcount > 0:
        return 'Site removed successfully!'
    else:
        return 'Something went wrong !'


def connect():
    try:
        connection = mysql.connector.connect(
            host="164.68.117.175",
            database="slackbot",
            user="outside",
            password="Bbr0s.1244"
        )

        if connection.is_connected():
            return connection
        else:
            return 0

    except Error as e:
        print("Error while connecting to mysql", e)


