import git
import os

base_path = '/home/webs/'


def pull_project(website):
    path = base_path + website
    if is_git_repo(path):

        repo = git.Repo(path)

        current = repo.head.commit
        repo.remotes.origin.pull()

        if current != repo.head.commit:
            return "Pull succesfull."
        else:
            return "Nothing new to pull."
    else:
        return path + " is not a valid path in the server!"


def npm_run(website):
    try:
        path = "/home/webs/{0}".format(website)
        dirFiles = os.listdir(path)
        if 'package.json' not in dirFiles:
            return path + " is not a npm directory"

        os.chdir(path)
        response = []

        response.append(os.popen('npm install').read())
        response.append(os.popen('npm run build').read())

        return response
    except FileNotFoundError:
        return path + "is not a valid path in the server!"


def is_git_repo(path):
    try:
        _ = git.Repo(path).git_dir
        return True
    except (git.exc.InvalidGitRepositoryError, git.exc.NoSuchPathError):
        return False
