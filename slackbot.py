import os
from datetime import date, datetime
import time
import re
from slackclient import SlackClient
import paramiko
from websites import *
from projecthandling import *

# instantiate Slack client
slack_client = SlackClient(
    'xoxb-5112708981-1257327237652-UL1vocghpKBwMkb9qZXeCANE')
# starterbot's user ID in Slack: value is assigned after the bot starts up
starterbot_id = None

# constants
RTM_READ_DELAY = 1  # 1 second delay between reading from RTM
EXAMPLE_COMMAND = "do"
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"

# employees with key as their name and their unique id as the value
employees = {}
employees['dorentina'] = 'U0WPS2VUK'
employees['egzoni'] = 'UPHR4FA0K'

# servers
servers = {}
servers['164'] = {}
servers['164']['host'] = "164.68.117.175"
servers['164']['port'] = 22
servers['164']['username'] = "root"
servers['164']['password'] = "Vps.113355!"


def connectToServerAndRunCommand(serverData, command):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(serverData['host'], serverData['port'],
                serverData['username'], serverData['password'])
    try:
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
        return 'Command executed successfully!'
    except:
        return 'Something went wrong :('


def dailyUpdates(message):
    message = 'Hey <!channel> ' + message
    updateBool = False
    slack_client.api_call(
        "chat.postMessage",
        channel='GNGDY2T3R',
        text=message
    )


def sendPrivateMessage(userID, message):
    updateBool = False
    slack_client.api_call(
        "chat.postMessage",
        channel=userID,
        text=message
    )


def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            user_id, message = parse_direct_mention(event["text"])
            if user_id == starterbot_id:
                return message, event["channel"], event['user'], event['type']
        elif event['type'] == "goodbye":
            return None, None, None, "goodbye"
    return None, None, None, None


def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)


def automatedMessage():
    if datetime.now().isoweekday() in range(1, 6):
        if now.hour == 9 and now.minute == 0:
            dailyUpdates("Daily Update Time!")

        if now.hour == 9 and now.minute == 45:
            dailyUpdates(
                "Ju lutem boni update para ores 10!")
        if now.hour == 16 and now.minute == 30:
            dailyUpdates("Update Claritask!")
        if now.hour == 17 and now.minute == 0:
            dailyUpdates(
                "Faleminderit shum per punen e palodhshme sot!")
        if now.hour == 14 and now.minute == 59:
            sendPrivateMessage(
                employees['dorentina'], "Tina, Mos harro Tenderat! \n Faleminderit :)")

    if now.hour == 8 and now.minute == 5:
        sendPrivateMessage(
            employees['egzoni'], 'Testing message')

    if now.hour == 14 and now.minute == 21:
        sendPrivateMessage(employees['egzoni'], 'Testing message')


def handle_command(command, channel, user):
    """
        Executes bot command if the command is known
    """
    # Default response is help text for the user
    default_response = "Command does not exist"

    # Finds and executes the given command, filling in response
    response = None
    # This is where you start to implement more commands!
    if command.lower().startswith('build 75mall'):
        response = connectToServerAndRunCommand(
            servers['164'], "cd /home/webs/75mall.bbros.al ; bash runbuild.sh")

    # This is where you start to implement more commands!
    if command.startswith(EXAMPLE_COMMAND):
        response = "Sure...write some more code then I can do that!"
    if command.lower().startswith('zani'):
        response = "Arbri ska lidhje me gjo"
    if command.lower().startswith('arbri'):
        response = "Arbri menon qe o devopsi ma i miri"
    if command.lower().startswith('ardit'):
        response = "Arditi programeri ma i mir nzyre"
    if command.lower().startswith('era'):
        response = "Ness Massness erinjo me audi a8 nborsh"
    if command.lower().startswith('nina'):
        response = "Nina o orizi ma i madh i prishtines!!!!"
    if command.lower().startswith('dako'):
        response = "KLIMA DHEZT KLIMA DHEZT"
    if command.lower().startswith('caluki'):
        response = "Ska bateri"
    if command.lower().startswith('test'):
        if now.hour == 14:
            response = 'boni';
        else:
            response = "Sboni"

    # Fun commands end here
    # Serious commands start here
    if command.lower().startswith('project'):
        commandArr = command.split()
        if len(commandArr) < 3:
            response = "Please input the correct form of the command"
        else:
            if commandArr[1] == 'pull':
                response = pull_project(commandArr[2])

            if commandArr[1] == 'npm':
                response = npm_run(commandArr[2])

                if isinstance(response, list):
                    for message in response:
                        slack_client.api_call(
                            "chat.postMessage",
                            channel=user,
                            text=message
                        )
                else:
                    slack_client.api_call(
                        "chat.postMessage",
                        channel=channel,
                        text=response
                    )

                return

    if command.startswith('server'):
        commandArr = command.split()

        if commandArr[1] == 'add':
            website = commandArr[2].split('//')[1].split('|')[0]
            response = add(website)
        elif commandArr[1] == 'remove':
            website = commandArr[2].split('//')[1].split('|')[0]
            response = remove(website)
        elif commandArr[1] == 'ping':
            if commandArr[2] == 'all':
                websites = allHosts()
                for website in websites:
                    response = ping(website[0])

                    slack_client.api_call(
                        "chat.postMessage",
                        channel=channel,
                        text=response
                    )

                return

        elif commandArr[1] == 'list':
            websites = allHosts()

            for website in websites:
                slack_client.api_call(
                    "chat.postMessage",
                    channel=channel,
                    text=website[0]
                )

            return

    # Sends the response back to the channel
    slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text=response or default_response
    )


if __name__ == "__main__":
    if slack_client.rtm_connect(with_team_state=False):
        print("Starter Bot connected and running!")
        # Read bot's user ID by calling Web API method `auth.test`
        starterbot_id = slack_client.api_call("auth.test")["user_id"]
        now = datetime.now()
        lastExecutedMinute = now.minute
        while True:
            command, channel, user, eventType = None, None, None, None

            try:
                command, channel, user, eventType = parse_bot_commands(
                    slack_client.rtm_read())
            except:
                slack_client = SlackClient(
                    'xoxb-5112708981-1257327237652-UL1vocghpKBwMkb9qZXeCANE')
                command, channel, user, eventType = parse_bot_commands(
                    slack_client.rtm_read())
                starterbot_id = slack_client.api_call("auth.test")["userid"]
                print("An exception occurred")

            now = datetime.now()
            if eventType == "goodbye":
                if slack_client.rtm_connect(with_team_state=False):
                    starterbot_id = slack_client.api_call("auth.test")[
                        "user_id"]

            if now.minute != lastExecutedMinute:
                lastExecutedMinute = now.minute
                automatedMessage()
            else:
                if command:
                    handle_command(command, channel, user)

            # if datetime.datetime.now().minute % 20 == 0:
            #     websites = allHosts()
            #     for website in websites:
            #         response = ping(website[0])
            #
            #         slack_client.api_call(
            #             "chat.postMessage",
            #             channel='C0183F3BQJC',
            #             text=response
            #         )

            time.sleep(RTM_READ_DELAY)
    else:
        print("Connection failed. Exception traceback printed above.")
